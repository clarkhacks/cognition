var fs = require('fs'),
  express = require("express"),
  app = express(),
  download = require('download-to-file'),
  captionbot = require('mediocre-captionbot'),
  titleCase = require('./app_modules/case.js'),
  shortid = require('shortid'),
  ngrok = require('ngrok'),
  push = require('git-push');

app.listen(3032, function() {
  console.log('Server Listening on 3032')
})

function startUp() {
  global.currentId = shortid.generate();
  global.savePath = '/public/image_recogniton/img/' + global.currentId + ".jpg";
  download('https:\/\/unsplash.it/400/200?random', __dirname + global.savePath, function(err) {
    if (err) throw err
    console.log( global.currentId + ' Downloaded from UnSplash.it')
    captionImage()
  })
}

function captionImage() {
  app.use("/public/image_recogniton/img/id", express.static(__dirname + '/public/image_recogniton/img/'));
  ngrok.connect(3032, function(err, url) {
    captionbot(url + '/public/image_recogniton/img/id/' + global.currentId + '.jpg')
      .then(caption => {
        global.caption = titleCase.titleCase(caption.replace(/ *\b\S*?I think *\b/g, ''));
        console.log("Image " + global.currentId + ": Caption: " + caption);
        writeData();
      })
  });
}

function writeData() {
  fs.appendFile(__dirname + '/public/image_recogniton/data.yml',
    "- image: " + global.savePath + "\n" + "caption: " + global.caption + "\n",
    function(err) {
      if (err) throw err;
      console.log('Data Written');
      uploadData();
    });
}
function uploadData() {
  push(__dirname + '/public', 'https://github.com/clarkhacks/Cognitive-Computing', function() {
    console.log('Pushed to Github');
  });
}



startUp();











// function writeData() {
//   fs.appendFile(__dirname + '/public/image_recogniton/data.yml',
//     "- image: " + global.savePath + "\n" + "\t" + "caption: " + global.caption + "\n",
//     function(err) {
//       if (err) throw err;
//       console.log('Data Written');
//       uploadData();
//     });
// }
