// ******Title Case******
exports.titleCase = function(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
